package fr.klock.dev.salon.salonapi.domain;

enum SlotStatus {
    AVAILABLE, LOCKED, CONFIRMED, CANCELLED
}
