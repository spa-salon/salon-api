package fr.klock.dev.salon.salonapi.repositories;

import fr.klock.dev.salon.salonapi.domain.SalonServiceDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalonServiceDetailRepository extends CrudRepository<SalonServiceDetail, Long> {
}
