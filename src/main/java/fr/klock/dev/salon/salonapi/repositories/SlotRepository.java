package fr.klock.dev.salon.salonapi.repositories;

import fr.klock.dev.salon.salonapi.domain.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface SlotRepository extends JpaRepository<Slot, Long> {

    Iterable<Slot> findAvailableSlotsForDateAndService(LocalDate requestedDate, Long salonServiceId);
}
