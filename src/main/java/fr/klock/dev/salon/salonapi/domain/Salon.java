package fr.klock.dev.salon.salonapi.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "salon")
@Data
@NoArgsConstructor
@ToString
public class Salon {
    private String name;
    private String address;
    private String city;
    private String state;
    private String zipcode;
    private String phone;
}
