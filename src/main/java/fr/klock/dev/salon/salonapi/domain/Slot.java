package fr.klock.dev.salon.salonapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@ToString
@NamedNativeQuery(name = "Slot.findAvailableSlotsForDateAndService",
        resultClass = Slot.class,
        query = "select s.*" +
                " from Slot s, slot_available_services sas, salon_service_detail detail" +
                " where s.id = sas.slot_id" +
                "   and sas.available_services_id = detail.id" +
                "   and s.status = 0" +
                "   and DATE(s.slot_for) = DATE(?)" +
                "   and detail.id = ?")
public class Slot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    Set<SalonServiceDetail> availableServices;

    @ManyToOne
    private SalonServiceDetail selectedService;

    String stylistName;


    LocalDateTime slotFor;

    private SlotStatus status;

    LocalDateTime lockedAt;
    LocalDateTime confirmedAt;
}

