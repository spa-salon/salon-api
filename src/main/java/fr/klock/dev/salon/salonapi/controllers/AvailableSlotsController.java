package fr.klock.dev.salon.salonapi.controllers;

import fr.klock.dev.salon.salonapi.domain.Slot;
import fr.klock.dev.salon.salonapi.repositories.SlotRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.cors.CorsConfiguration;

import java.time.LocalDate;

@RestController
@RequestMapping(produces = "application/json")
@CrossOrigin(origins = CorsConfiguration.ALL)
public class AvailableSlotsController {

    @Autowired
    SlotRepository slotRepository;

    @GetMapping(path = "/api/slots/retrieveAvailableSlots/{salonServiceId}/{formattedDate}")
    @ApiOperation(value = "RetrieveAvailableSlotForAServiceAPI")
    @ApiResponses(
            @ApiResponse(code = 200,
                    message = "OK",
                    examples = @Example(@ExampleProperty(mediaType = "application/json",
                            value = "[\n" +
                                    "  {\n" +
                                    "    \"id\": 61,\n" +
                                    "    \"selectedService\": null,\n" +
                                    "    \"stylistName\": \"George Wagner\",\n" +
                                    "    \"slotFor\": \"2021-03-25T10:00:00\",\n" +
                                    "    \"status\": \"AVAILABLE\",\n" +
                                    "    \"lockedAt\": null,\n" +
                                    "    \"confirmedAt\": null\n" +
                                    "  }," +
                                    "]"))))
    public Iterable<Slot> retrieveAvailableSlots(
            @ApiParam(value = "Id of the salon service")
            @PathVariable("salonServiceId") Long salonServiceId,

            @ApiParam(defaultValue = "2021-03-25", value = "Date in yyyy-MM-dd format")
            @PathVariable("formattedDate") String requestedAppointmentDateString) {
        LocalDate requestedAppointmentDate = LocalDate.parse(requestedAppointmentDateString);
        return slotRepository.findAvailableSlotsForDateAndService(requestedAppointmentDate, salonServiceId);
    }
}
