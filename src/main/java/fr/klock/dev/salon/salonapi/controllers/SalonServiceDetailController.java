package fr.klock.dev.salon.salonapi.controllers;

import fr.klock.dev.salon.salonapi.domain.SalonServiceDetail;
import fr.klock.dev.salon.salonapi.repositories.SalonServiceDetailRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.cors.CorsConfiguration;

@RestController
@RequestMapping(produces = "application/json")
@CrossOrigin(origins = CorsConfiguration.ALL)
public class SalonServiceDetailController {

    @Autowired
    SalonServiceDetailRepository salonServiceDetailRepository;

    @GetMapping("/api/services/retrieveAvailableSalonServices")
    @ApiOperation(value = "RetrieveAvailableSalonServicesAPI")
    public Iterable<SalonServiceDetail> getSalonServiceDetail() {
        return salonServiceDetailRepository.findAll();
    }
}
